PKGS=(ls -1 packages)

bootstrap:
	lerna bootstrap

reinstall:
	rm -rf packages/*/node_modules
	lerna bootstrap

version:
	lerna version

changed:
	lerna changed

ls:
	lerna ls

diff:
	lerna diff

generate:
	./scripts/generate.sh

publish:
	lerna publish
