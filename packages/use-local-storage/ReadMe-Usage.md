# useLocalStorage #

A React Hook which sets/gets a value in window.localStorage.

## Synopsis ##

In your React component:

```
import { useState } from 'react'
import useLocalStorage from "use-local-storage"

function Skeleton() {
  // set the value in localStorage upon initialisation
  const [ salutation, setSalutation ] = useLocalStorage("salutation", "Hello")

  // don't set the value but instead retrieve what is in localStorage()
  const [ name, setName ] = useLocalStorage("name")

  return <p>{ salutation } { name }</p>
}
```

