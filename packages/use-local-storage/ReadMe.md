<div align="center" style="text-align: center;">
    <h2>
      <a href="https://reacthooks.org/"><span style="color: yellow;">✴</span> Project Homepage <span style="color: yellow;">✴</span></a>
    </h2>
    <p>
      <a href="https://reacthooks.org/"><img style="width: 256px; height: 256px;" src="https://i.postimg.cc/SRRmYPpP/logo-on-trans.png" /></a>
    </p>
    <h3>
      <a href="https://reacthooks.org/use-local-storage"><span style="color: red;">✴</span> Docs <span style="color: red;">✴</span></a>
    </h3>
    <p>
      <a href="https://reacthooks.org/use-local-storage#demo"><span style="color: red;">✴</span> Demo <span style="color: red;">✴</span></a>
      <br />
      <a href="https://reacthooks.org/use-local-storage#usage"><span style="color: red;">✴</span> Usage <span style="color: red;">✴</span></a>
      <br />
      <a href="https://reacthooks.org/use-local-storage#examples"><span style="color: red;">✴</span> Examples <span style="color: red;">✴</span></a>
    </p>
</div>

## Install ##

```
$ npm install @reacthooks.org/use-local-storage
```

# useLocalStorage #

A React Hook which sets/gets a value in window.localStorage.

## Synopsis ##

In your React component:

```
import { useState } from 'react'
import useLocalStorage from "use-local-storage"

function Skeleton() {
  // set the value in localStorage upon initialisation
  const [ salutation, setSalutation ] = useLocalStorage("salutation", "Hello")

  // don't set the value but instead retrieve what is in localStorage()
  const [ name, setName ] = useLocalStorage("name")

  return <p>{ salutation } { name }</p>
}
```

## Other Hooks ##

Please see all of the other reacthooks.org hooks:

* [use-document-title](https://www.npmjs.com/package/@reacthooks.org/use-document-title) - Change the `document.title`
* [use-resize](https://www.npmjs.com/package/@reacthooks.org/use-resize) - Subscribe to Window 'resize' events and get the width and height
* [use-window-width](https://www.npmjs.com/package/@reacthooks.org/use-window-width) - Subscribe to Window 'resize' events and get the width
* [use-online](https://www.npmjs.com/package/@reacthooks.org/use-online) - Get online/offline status
* [use-match-media](https://www.npmjs.com/package/@reacthooks.org/use-match-media) - Get whether a media query is matched
  * `.usePrefersColorScheme()` - Get whether the user prefers the 'light' or 'dark' color scheme
* [use-set-timeout](https://www.npmjs.com/package/@reacthooks.org/use-set-timeout) - use and automatically clear a `setTimeout()`
* [use-set-interval](https://www.npmjs.com/package/@reacthooks.org/use-set-interval) - use and automatically clear a `setInterval()`
* [use-orientation-change](https://www.npmjs.com/package/@reacthooks.org/use-orientation-change) - get Device Orientation updates
* [use-session-storage](https://www.npmjs.com/package/@reacthooks.org/use-session-storage) - gets and sets a key in window.sessionStorage
* [use-local-storage](https://www.npmjs.com/package/@reacthooks.org/use-local-storage) - gets and sets a key in window.localStorage
* [use-form-validation](https://www.npmjs.com/package/@reacthooks.org/use-form-validation) - helps manage form values, validation, and errors
* [use-watch-position](https://www.npmjs.com/package/@reacthooks.org/use-watch-position) - get the user's location using the Geolocation API

## Author ##

```
$ npx chilts

   ╒════════════════════════════════════════════════════╕
   │                                                    │
   │   Andrew Chilton (Personal)                        │
   │   -------------------------                        │
   │                                                    │
   │          Email : andychilton@gmail.com             │
   │            Web : https://chilts.org                │
   │        Twitter : https://twitter.com/andychilton   │
   │         GitHub : https://github.com/chilts         │
   │         GitLab : https://gitlab.org/chilts         │
   │                                                    │
   │   Apps Attic Ltd (My Company)                      │
   │   ---------------------------                      │
   │                                                    │
   │          Email : chilts@appsattic.com              │
   │            Web : https://appsattic.com             │
   │        Twitter : https://twitter.com/AppsAttic     │
   │         GitLab : https://gitlab.com/appsattic      │
   │                                                    │
   │   Node.js / npm                                    │
   │   -------------                                    │
   │                                                    │
   │        Profile : https://www.npmjs.com/~chilts     │
   │           Card : $ npx chilts                      │
   │                                                    │
   ╘════════════════════════════════════════════════════╛

```

(Ends)
