import React, { useState, useEffect } from 'react'

export default function useSessionStorage(key, val) {
  const [ value, setValue ] = useState(val || window.localStorage.getItem(key))

  useEffect(() => {
    window.localStorage.setItem(key, value)
  }, [ value ])

  return [ value, setValue ]
}
