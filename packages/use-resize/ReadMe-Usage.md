# useResize #

React (State and Effect) Hook to get your window size.

## Synopsis ##

In your React component:

```
import useResize from "use-resize"

function SizeInfo() {
  const size = useResize()

  return <p>size = ({ size.width }, { size.height })</p>
}
```

There is no need to subscribe or unsubscribe to the window `'resize'` event
since this hook does it for you.

