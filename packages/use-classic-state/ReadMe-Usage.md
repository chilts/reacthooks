# useClassicState #

A React Hook which gives you the classic `this.setState({ ... })`.

(But not `setState(fn)`.)

## Synopsis ##

In your React component:

```
import { useState } from 'react'
import useClassicState from "use-classic-state"

function Skeleton() {
  const [ request, setRequest ] = useClassicState({
    loading: false,
    result: null
  })

  // then inside your Ajax request hook
  setRequest({
    loading: true,
  })

  // and once the request has returned
  setRequest({
    loading: false,
    result: { ... },
  })
}
```

