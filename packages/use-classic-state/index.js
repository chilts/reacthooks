import React, { useState } from 'react'
const isPlainObject = require('lodash.isplainobject')

export default function useClassicState(initialState) {
  const [ state, setState ] = useState(initialState)

  // check the initial state is okay
  if (initialState && !isPlainObject(initialState)) {
    throw new Error("Initial state must be an object.")
  }

  function setClassicState(updates) {
    setState(Object.assign({}, state, updates))
  }

  return [ state, setClassicState ]
}
