# useWindowWidth #

React (State and Effect) Hook to get your window width.

## Synopsis ##

In your React component:

```
import useWindowWidth from "use-window-width";

function WidthInfo() {
  const width = useWindowWidth();

  return <code>width = { width }</code>
}
```

There is no need to subscribe or unsubscribe to the window `'resize'` event
since this hook does it for you.
