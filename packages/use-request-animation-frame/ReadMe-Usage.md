# useSkeleton #

A React Hook (using useEffect) which ... skeleton ....

## Synopsis ##

In your React component, ... skeleton ...:

```
import { useState } from 'react'
import useSkeleton from "use-skeleton"

function Skeleton() {
  // Skeleton

  return <p>Count = { count }</p>
}
```

