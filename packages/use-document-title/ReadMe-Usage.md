# useDocumentTitle #

React Hook (using `useEffect()`) to set your document title.

## Synopsis ##

In your React component:

```
import useDocumentTitle from "use-document-title";

function App() {
  useDocumentTitle('My Title - which could come from state');

  return <div>...</div>;
}
```

