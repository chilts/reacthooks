# useSetTimeout #

A React Hook (using useEffect) which uses window.setTimeout().

## Synopsis ##

In your React component:

```
import { useState } from 'react'
import useSetTimeout from "use-set-timeout"

function SetTimeout() {
  const [ triggered, setTriggered ] = useState(false)

  useSetTimeout(() => { setTriggered(true) }, 5 * 1000)

  return <p>triggered = { triggered }</p>
}
```

There is no need to keep a reference to the timeout ID since it will be automatically cleared when the component unmounts.

Please note that the timeout is cleared and reset if you change the timeout `ms` but not if you change the function `fn`.

## Usage ##

```
useSetTimeout(fn, delay)
```

* `fn` is a function to execute
* `delay` is the delay in milliseconds

