# useOrientationChange #

React (State and Effect) Hook to get your device orientation.

## Synopsis ##

In your React component:

```
import useOrientationChange from "use-orientation-change";

function OrientationChange() {
  const orientation = useOrientationChange()

  // 'portrait' or 'landscape'
  return <code>orientation = { orientation }</code>
}
```

There is no need to subscribe or unsubscribe to the window `'orientationchange'` event
since this hook does it for you.

