# useSetInterval #

A React Hook (using useEffect) which uses window.setInterval().

Note: this was published and didn't work a few days before Dan published this
[Making setInterval Declarative with React Hooks](https://overreacted.io/making-setinterval-declarative-with-react-hooks/),
which saved my bacon. Many thanks.

## Synopsis ##

In your React component, increment a counter every second:

```
import { useState } from 'react'
import useSetInterval from "use-set-interval"

function SetInterval() {
  const [ count, setCount ] = useState(0)

  useSetInterval(() => setCount(count + 1), 1000)

  return <p>Count = { count }</p>
}
```

There is no need to keep a reference to the interval ID since it will be automatically cleared when the component unmounts.

Please note that the interval is cleared and reset if you change the interval `ms` but not if you change the function `fn`. This is a good thing, since it allows you to change the interval in a declarative way, instead of having to fiddle with the interval IDs yourself.

## Usage ##

```
useSetInterval(fn, delay)
```

* `fn` is a function to execute
* `delay` is the delay between executions in milliseconds

