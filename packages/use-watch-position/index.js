import { useEffect } from 'react'

export default function useWatchPosition(fn) {
  if (!navigator.geolocation) {
    fn(new Error("Geolocation API not supported"))
    return
  }

  useEffect(() => {
    const watcher = navigator.geolocation.watchPosition(
      pos => { fn(null, pos) },
      err => { fn(err) }
    )

    return () => {
      navigator.geolocation.clearWatch(watcher)
    }
  }, [])
}
