# useWatchPosition #

A React Hook (using useEffect) which uses the Geolocation API, specifically `navigator.geolocation.watchPosition()`.

## Synopsis ##

In your React component:

```
import { useState } from 'react'
import useSetTimeout from "use-set-timeout"

function SetTimeout() {
  const [ position, setPosition ] = useState(false)

  useWatchPosition((err, newPosition) => {
    if (err) {
      // either the user refused access, or something bad happened
      // * err.code === 1 // Permission Denied
      // * err.code === 2 // Position Unavailable
      // * err.code === 3 // Timeout
      // * no err.code    // some other error!
    }
    setPosition(newPosition)
  })

  return (
    <p>latitude = { position && position.coords && position.coords.latitude }</p>
    <p>longitude = { position && position.coords && position.coords.logitude }</p>
  )
}
```
