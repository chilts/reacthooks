# useSessionStorage #

A React Hook which sets/gets a value in window.sessionStorage.

## Synopsis ##

In your React component:

```
import { useState } from 'react'
import useSessionStorage from "use-session-storage"

function Skeleton() {
  // set the value in sessionStorage upon initialisation
  const [ salutation, setSalutation ] = useSessionStorage("salutation", "Hello")

  // don't set the value but instead retrieve what is in sessionStorage()
  const [ name, setName ] = useSessionStorage("name")

  return <p>{ salutation } { name }</p>
}
```

