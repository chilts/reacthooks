# useOnline #

React (State and Effect) Hook to get your online status.

## Synopsis ##

In your React component:

```
import useOnline from "use-online"

function OnlineInfo() {
  const online = useOnline()

  return <code>online = { online }</code>
}
```

There is no need to subscribe or unsubscribe to the window `'online'` or
`'offline'` events since this hook does it for you.

