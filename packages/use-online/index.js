import { useState, useEffect } from 'react'

// Docs : https://developer.mozilla.org/en-US/docs/Web/API/NavigatorOnLine/onLine
// Demo : http://html5-demos.appspot.com/static/navigator.onLine.html

export default function useOnline() {
  const [ online, setOnline ] = useState(window.navigator.onLine)

  useEffect(() => {
    const handleOnline = () => setOnline(true)
    const handleOffline = () => setOnline(false)

    window.addEventListener('online', handleOnline)
    window.addEventListener('offline', handleOffline)

    return () => {
      window.removeEventListener('online', handleOnline)
      window.removeEventListener('offline', handleOffline)
    }
  }, [])

  return online
}
