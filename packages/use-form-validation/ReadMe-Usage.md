# useFormValidation #

React Hook to help with forms.

## Usage ##

```js
import useFormValidation from 'use-form-validation';

const initialFormState = {
  email: '',
}
function validate(values) {
  console.log('validate()')
  // check email address
  return { email: 'Invalid email address' }
}

function FormValidation() {
  const { values, errors, onChange, onBlur, onSubmit } = useFormValidation(initialFormState, validate, values => {
    // called when there has been a change and everything validaates
    console.log('values:', values)
  })

  return (
    <form onSubmit={ onSubmit }>
      <label>
        <span>Email</span>
        <input name="email" value={ values.email } onBlur={ onBlur } onChange={ onChange } />
      </label>
      <br />
      {
        errors.email && <p style={ { color: 'red' } }>{ errors.email }</p>
      }
      <input type="submit" />
    </form>
  );
}
```

