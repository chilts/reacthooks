import { useState, useEffect } from 'react'

export default function useFormValidation(initialState, validate, submit) {
  const [ values, setValues ] = useState(initialState)
  const [ errors, setErrors ] = useState({})

  function onChange(ev) {
    setValues({
      ...values,
      [ev.target.name]: ev.target.value,
    })
  }

  function onBlur() {
    setErrors(validate(values))
  }

  function onSubmit(ev) {
    ev.preventDefault()

    const currentErrors = validate(values)
    setErrors(currentErrors)

    console.log('values:', values)
    console.log('errors:', errors)

    // use `currentErrors` instead of `errors` from `useState()` since that'll be old
    if (Object.keys(currentErrors).length) {
      return
    }
    submit(ev, values)
  }

  return {
    values,
    errors,
    onChange,
    onBlur,
    onSubmit,
  }
}
