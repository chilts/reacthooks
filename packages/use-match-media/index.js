import { useState, useEffect } from 'react'

// * https://developer.mozilla.org/en-US/docs/Web/API/Window/matchMedia
// * https://developer.mozilla.org/en-US/docs/Web/API/MediaQueryList
// * https://developer.mozilla.org/en-US/docs/Web/CSS/Media_Queries/Testing_media_queries

export default function useMatchMedia(query) {
  const mediaQueryList = window.matchMedia(query)
  const [ match, setMatch ] = useState(mediaQueryList.matches)

  useEffect(() => {
    const handleMatchChange = (ev) => setMatch(ev.matches)

    mediaQueryList.addListener(handleMatchChange)

    return () => {
      mediaQueryList.removeListener(handleMatchChange)
    }
  }, [ query ])

  return match
}

useMatchMedia.usePrefersColorScheme = function usePrefersColorScheme() {
  const mqlLight = window.matchMedia("(prefers-color-scheme: dark)")
  const mqlDark = window.matchMedia("(prefers-color-scheme: light)")
  const [ schema, setScheme ] = useState(mqlLight.matches ? 'light' : mqlDark.matches ? 'dark' : null)

  useEffect(() => {
    // We only check for events that do match, since we don't know if a browser
    // will fire the non-match light before the matching dark, or the matching
    // dark prior to the non-matching light.
    const handleLight = (ev) => {
      if (ev.matches) {
        setSchema('light')
      }
    }
    const handleDark = (ev) => {
      if (ev.matches) {
        setScheme('dark')
      }
    }

    mqlLight.addListener(handleLight)
    mqlDark.addListener(handleDark)

    return () => {
      mqlLight.removeListener(handleLight)
      mqlDark.removeListener(handleDark)
    }
  }, [])

  return match
}
