# useMatchMedia #

React (State and Effect) Hook to check any media matches.

This IS NOT JUST `.matchMedia()` though since we also provide easy to use
built-in matches such as the following:

* `useMatchMedia.prefersColorScheme() // returns 'light' or 'dark' (or null)`

## Synopsis ##

In your React component:

```
import useMatchMedia from "use-match-media";

function MediaMatchSmall() {
  const small = useMediaMatch("(max-width: 600px)")

  return <code>Small Device = { small }</code>
}
```

There is no need to subscribe or unsubscribe to a `'window.matchMedia()'
`MediaQueryList`` event since this hook does it for you.

## `.usePrefersColorScheme()` ##

We provide a simple predefined way to obtain whether the user prefer the
'light' or 'dark' color scheme. If this isn't supported by the browser, then
it'll always return `null`.

```
import useMatchMedia from "use-match-media";

function ColorScheme() {
  const scheme = useMediaMatch.usePrefersColorScheme()

  return <p>You prefer the <strong>{ scheme }</strong> color scheme.</p>
}
```

