<div align="center" style="text-align: center;">
    <h2>
      <a href="https://reacthooks.org/"><span style="color: yellow;">✴</span> Project Homepage <span style="color: yellow;">✴</span></a>
    </h2>
    <p>
      <a href="https://reacthooks.org/"><img style="width: 256px; height: 256px;" src="https://i.postimg.cc/SRRmYPpP/logo-on-trans.png" /></a>
    </p>
    <h3>
      <a href="https://reacthooks.org/__PACKAGE_NAME__"><span style="color: red;">✴</span> Docs <span style="color: red;">✴</span></a>
    </h3>
    <p>
      <a href="https://reacthooks.org/__PACKAGE_NAME__#demo"><span style="color: red;">✴</span> Demo <span style="color: red;">✴</span></a>
      <br />
      <a href="https://reacthooks.org/__PACKAGE_NAME__#usage"><span style="color: red;">✴</span> Usage <span style="color: red;">✴</span></a>
      <br />
      <a href="https://reacthooks.org/__PACKAGE_NAME__#examples"><span style="color: red;">✴</span> Examples <span style="color: red;">✴</span></a>
    </p>
</div>

