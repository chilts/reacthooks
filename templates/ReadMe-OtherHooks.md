## Other Hooks ##

Please see all of the other reacthooks.org hooks:

* [use-document-title](https://www.npmjs.com/package/@reacthooks.org/use-document-title) - Change the `document.title`
* [use-resize](https://www.npmjs.com/package/@reacthooks.org/use-resize) - Subscribe to Window 'resize' events and get the width and height
* [use-window-width](https://www.npmjs.com/package/@reacthooks.org/use-window-width) - Subscribe to Window 'resize' events and get the width
* [use-online](https://www.npmjs.com/package/@reacthooks.org/use-online) - Get online/offline status
* [use-match-media](https://www.npmjs.com/package/@reacthooks.org/use-match-media) - Get whether a media query is matched
  * `.usePrefersColorScheme()` - Get whether the user prefers the 'light' or 'dark' color scheme
* [use-set-timeout](https://www.npmjs.com/package/@reacthooks.org/use-set-timeout) - use and automatically clear a `setTimeout()`
* [use-set-interval](https://www.npmjs.com/package/@reacthooks.org/use-set-interval) - use and automatically clear a `setInterval()`
* [use-orientation-change](https://www.npmjs.com/package/@reacthooks.org/use-orientation-change) - get Device Orientation updates
* [use-session-storage](https://www.npmjs.com/package/@reacthooks.org/use-session-storage) - gets and sets a key in window.sessionStorage
* [use-local-storage](https://www.npmjs.com/package/@reacthooks.org/use-local-storage) - gets and sets a key in window.localStorage
* [use-form-validation](https://www.npmjs.com/package/@reacthooks.org/use-form-validation) - helps manage form values, validation, and errors
* [use-watch-position](https://www.npmjs.com/package/@reacthooks.org/use-watch-position) - get the user's location using the Geolocation API

