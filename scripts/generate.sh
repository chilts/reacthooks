#!/bin/bash

PKGS=$(ls -1 packages/)

for PKG in $PKGS; do
		echo "Generating $PKG ..."
		m4 --prefix-builtins              \
       --define=__PACKAGE_NAME__=$PKG \
       templates/ReadMe-M4.md       \
       templates/ReadMe-Logo.md       \
       templates/ReadMe-Install.md    \
       packages/$PKG/ReadMe-Usage.md  \
       templates/ReadMe-OtherHooks.md \
       templates/ReadMe-Author.md     \
       templates/ReadMe-Ends.md       \
       > packages/$PKG/ReadMe.md
done
